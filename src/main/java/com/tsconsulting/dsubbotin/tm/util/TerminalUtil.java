package com.tsconsulting.dsubbotin.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        return Integer.parseInt(SCANNER.nextLine());
    }

}
