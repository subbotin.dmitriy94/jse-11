package com.tsconsulting.dsubbotin.tm.api.controller;

import com.tsconsulting.dsubbotin.tm.model.Project;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void showById();

    void showByIndex();

    void showByName();

    void showProject(final Project project);

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

}
