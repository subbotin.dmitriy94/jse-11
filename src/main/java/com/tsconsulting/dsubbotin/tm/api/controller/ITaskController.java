package com.tsconsulting.dsubbotin.tm.api.controller;

import com.tsconsulting.dsubbotin.tm.model.Task;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void showById();

    void showByIndex();

    void showByName();

    void showTask(final Task task);

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

}
