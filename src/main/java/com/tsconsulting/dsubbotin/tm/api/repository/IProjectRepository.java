package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(final Project project);

    void remove(final Project project);

    List<Project> findAll();

    void clear();

    Project findById(final String id);

    Project findByIndex(final int index);

    Project findByName(final String name);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

}
