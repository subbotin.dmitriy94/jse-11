package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(final String name);

    void create(final String name, final String description);

    void add(final Project project);

    void remove(final Project project);

    List<Project> findAll();

    void clear();

    Project findById(final String id);

    Project findByIndex(final int index);

    Project findByName(final String name);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final int index, final String name, final String description);

}
