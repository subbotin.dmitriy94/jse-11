package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(final String name);

    void create(final String name, final String description);

    void add(final Task task);

    void remove(final Task task);

    List<Task> findAll();

    void clear();

    Task findById(final String id);

    Task findByIndex(final Integer index);

    Task findByName(final String name);

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final int index, final String name, final String description);

}
