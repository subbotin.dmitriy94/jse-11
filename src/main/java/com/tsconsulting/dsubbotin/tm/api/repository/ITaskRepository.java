package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(final Task task);

    void remove(final Task task);

    List<Task> findAll();

    void clear();

    Task findById(final String id);

    Task findByIndex(final int index);

    Task findByName(final String name);

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

}
