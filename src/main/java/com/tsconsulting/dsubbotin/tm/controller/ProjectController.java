package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.IProjectController;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[Create]");
    }

    @Override
    public void showProjects() {
        if (projectService.findAll().isEmpty()) System.out.println("[List projects empty]");
        int index = 1;
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(index++ + ". " + project);
    }

    @Override
    public void clearProjects() {
        projectService.clear();
        System.out.println("[Clear]");
    }

    @Override
    public void showById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Incorrect value]");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Incorrect value]");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("[Incorrect value]");
            return;
        }
        showProject(project);
    }

    @Override
    public void showProject(final Project project) {
        System.out.println("Id: " + project.getId() + "\n" +
                "Name: " + project.getName() + "\n" +
                "Description: " + project.getDescription());
    }

    @Override
    public void removeById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[Incorrect value]");
        else System.out.println("[Project removed]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[Incorrect value]");
        else System.out.println("[Project removed]");
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[Incorrect value]");
        else System.out.println("[Project removed]");
    }

    @Override
    public void updateById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (projectService.findById(id) == null) {
            System.out.println("[Incorrect value]");
            return;
        }
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        if (projectService.updateById(id, name, description) == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated project]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (projectService.findByIndex(index) == null) {
            System.out.println("[Incorrect value]");
            return;
        }
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        if (projectService.updateByIndex(index, name, description) == null) System.out.println("[Incorrect value]");
        else System.out.println("[Updated project]");
    }
}